import 'package:flutter/material.dart';
import 'package:wed_guru/widget/verticaltext.dart';
import 'package:wed_guru/widget/inputEmail.dart';
import 'package:wed_guru/widget/password.dart';
import 'package:wed_guru/widget/button.dart';
import 'package:wed_guru/widget/first.dart';
import 'package:wed_guru/widget/textLogin.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("lib/img/love.jpg"),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
          // gradient: LinearGradient(
          //     begin: Alignment.topRight,
          //     end: Alignment.bottomLeft,
          //     colors: [Colors.blue, Colors.lightBlueAccent]),
        ),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(children: <Widget>[
                  VerticalText(),
                  TextLogin(),
                ]),
                InputEmail(),
                PasswordInput(),
                ButtonLogin(),
                const FirstTime(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

