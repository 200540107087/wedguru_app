import 'package:flutter/material.dart';
import 'package:wed_guru/widget/signup.dart';
import 'package:wed_guru/widget/newEmail.dart';
import 'package:wed_guru/widget/buttonNewUser.dart';
import 'package:wed_guru/widget/password.dart';
import 'package:wed_guru/widget/newName.dart';
import 'package:wed_guru/widget/textNew.dart';
import 'package:wed_guru/widget/userold.dart';

class NewUser extends StatefulWidget {
  const NewUser({Key? key}) : super(key: key);

  @override
  State<NewUser> createState() => _NewUserState();
}

class _NewUserState extends State<NewUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("lib/img/love.jpg"),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
          // gradient: LinearGradient(
          //   begin: Alignment.topRight,
          //   end: Alignment.bottomLeft,
          //   colors: [Colors.blue,Colors.lightBlueAccent],
          // ),
        ),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SignUp(),
                    TextNew(),
                  ],
                ),
                NewName(),
                NewEmail(),
                PasswordInput(),
                ButtonNewUser(),
                UserOld(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
