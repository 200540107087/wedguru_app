import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children:<Widget> [
          ],
        ),
      ),
      bottomNavigationBar: Container(
        color: Colors.black,
        child:  Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0,
              vertical: 12),
          child: GNav(
            backgroundColor: Colors.black,
            color: Colors.white,
            activeColor: Colors.white,
            tabBackgroundColor: Colors.grey,
            padding: const EdgeInsets.all(18),
            gap: 8,
            tabs: [
              GButton(
                icon: Icons.home,
                text: 'Home',
                onPressed: () {},
              ),
              GButton(icon: Icons.favorite,
                text: 'Likes',
                onPressed: () {},),
              GButton(icon: Icons.add,
                text: 'Add',
                onPressed: () {},),
              GButton(icon: Icons.settings,
                text: 'Settings',
                onPressed: () {},),
            ],
          ),
        ),
      ),
    );
  }
}
