import 'package:flutter/material.dart';
import 'package:wed_guru/pages/login.dart';
class UserOld extends StatelessWidget {
  const UserOld({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.only(top:140,left: 110),
      child: Container(
        alignment: Alignment.topRight,
        height: 20,
        child: Row(
          children: <Widget>[
           const  Text(
              'Have we Met Before?',
              style: TextStyle(
                fontSize: 14,
                color: Colors.white70
              ),
            ),
            FloatingActionButton(
              backgroundColor: Colors.transparent,
              onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
            }, child:const  Text(
              'sign in',
              style: TextStyle(
                fontSize: 14,
                color:Colors.white,
              ),
              textAlign: TextAlign.right,
            ),
            )
          ],
        ),
      ),
    );
  }
}
