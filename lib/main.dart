import 'package:flutter/material.dart';
import 'package:wed_guru/pages/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   MaterialApp(
      title: 'Wed Guru',
      home: LoginPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
